# Golang Demo - Cards
---
This is a basic demonstration using Golang [v1.21](https://pkg.go.dev/os@go1.21.0?tab=versions) to create, shuffle, and deal a deck of cards. Deck functionality is asserted using a series of tests.

This project also provides a basic demonstration of file IO using the [os](https://pkg.go.dev/os@go1.21.0) package functions [WriteFile()](https://pkg.go.dev/os@go1.21.0#WriteFile) and [ReadFile()](https://pkg.go.dev/os@go1.21.0#ReadFile).
