package cards

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
)

type Deck []Card

type DeckFile struct {
	Owner string `json:"owner,omitempty"`
	Deck  Deck   `json:"deck"`
}

func (deck Deck) Deal(handsize int) (Deck, Deck) {
	return deck[:handsize], deck[handsize:]
}

func (deck Deck) DealHands(handCount int, handSize int) ([]Deck, Deck) {
	// Create our list of hands to be dealt
	var hands []Deck
	for hIdx := 0; hIdx < handCount; hIdx += 1 {
		hands = append(hands, make(Deck, 0, handSize))
	}

	// Determine the total length of cards to be distributed
	distMult := handCount * handSize
	// If the distribution count is less than the total number of cards in our deck...
	if distMult <= len(deck) {
		// Slice
		toDist, remaining := deck[:distMult], deck[distMult:]

		distIdx := 0
		for _, card := range toDist {
			// Add this card to the appropriate hand...
			hands[distIdx] = append(hands[distIdx], card)

			// Increment our distribution index
			distIdx += 1
			// but keep it within bounds of the hands we are distributing to
			if distIdx >= handCount {
				distIdx = 0
			}
		}

		// Our deck is the remaining slice
		deck = remaining
	}

	return hands, deck
}

// Prints the current deck of cards using human-readable card names
func (deck Deck) Print() {
	for _, card := range deck {
		fmt.Println(card.Face())
	}
}

// Creates a new deck of cards and returns it
func CreateDeck() Deck {
	var deck Deck

	suites := []CardSuit{Clubs, Diamonds, Hearts, Spades}
	ranks := []CardRank{Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King}

	for _, suit := range suites {
		for _, rank := range ranks {
			card := Card{suit, rank}

			deck = append(deck, card)
		}
	}

	return deck
}

// Attempts to read in a deck from a given filename
func DeckFromFile(fileName string) Deck {
	var deck Deck

	jBytes, rErr := os.ReadFile(fileName)
	if rErr != nil {
		fmt.Printf("Error reading deck file: %s\n", fileName)
		fmt.Println("Error: ", rErr)
	}

	var deckFile DeckFile
	mErr := json.Unmarshal(jBytes, &deckFile)
	if mErr != nil {
		fmt.Printf("Error decoding deck file: %s\n", fileName)
		fmt.Println("Error: ", mErr)
	} else {
		deck = deckFile.Deck
	}

	return deck
}

func (deck Deck) DeckToFile(fileName string, owner string) {
	// Create our deck file from the deck and use JSON marshall to encode it
	deckFile := DeckFile{Deck: deck, Owner: owner}

	jBytes, mErr := json.Marshal(deckFile)
	if mErr != nil {
		fmt.Printf("Error encoding deck file: %s\n", fileName)
		fmt.Println("Error: ", mErr)

		return
	}

	if !json.Valid(jBytes) {
		fmt.Println("Invalid json defined while creating deck file contents")

		return
	}

	wErr := os.WriteFile(fileName, jBytes, 0644)
	if wErr != nil {
		fmt.Printf("Error writing deck file: %s\n", fileName)
		fmt.Println("Error: ", wErr)

		return
	}
}

func (deck Deck) Shuffle() {
	deckLen := len(deck)
	for currentIndex := range deck {
		newIndex := rand.Intn(deckLen - 1)

		deck[currentIndex], deck[newIndex] = deck[newIndex], deck[currentIndex]
	}
}
