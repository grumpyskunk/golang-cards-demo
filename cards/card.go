package cards

// Represents a typical playing card as is defined by its suit and rank
type Card struct {
	Suit CardSuit `json:"suit"`
	Rank CardRank `json:"rank"`
}

type CardSuit int64

const (
	Joker CardSuit = iota
	Clubs
	Diamonds
	Hearts
	Spades
)

type CardRank int64

const (
	None CardRank = iota
	Ace
	Two
	Three
	Four
	Five
	Six
	Seven
	Eight
	Nine
	Ten
	Jack
	Queen
	King
)

// Return card suit as human-readable string
func (suit CardSuit) String() string {
	switch suit {
	case Clubs:
		return "Clubs"
	case Diamonds:
		return "Diamonds"
	case Hearts:
		return "Hearts"
	case Spades:
		return "Spades"
	}

	return ""
}

// Return card rank as human-readable string
func (rank CardRank) String() string {

	switch rank {
	case Ace:
		return "Ace"
	case Two:
		return "Two"
	case Three:
		return "Three"
	case Four:
		return "Four"
	case Five:
		return "Five"
	case Six:
		return "Six"
	case Seven:
		return "Seven"
	case Eight:
		return "Eight"
	case Nine:
		return "Nine"
	case Ten:
		return "Ten"
	case Jack:
		return "Jack"
	case Queen:
		return "Queen"
	case King:
		return "King"
	}

	return ""
}

// Returns the human-readable name of the card object
func (card Card) Face() string {
	suit := card.Suit.String()
	rank := card.Rank.String()
	union := " of "

	if rank == "" {
		union = ""
	}

	return rank + union + suit
}
