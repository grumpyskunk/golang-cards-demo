package cards

import (
	"testing"
)

func TestCreateDeck(test *testing.T) {
	deck := CreateDeck()

	// New deck should be 52 cards in length
	if len(deck) != 52 {
		test.Errorf("Expected deck length 52, but got: %v", len(deck))
	}

	// The first card should be of suit Clubs
	if deck[0].Suit != Clubs {
		card := deck[0]
		test.Errorf("Expected first deck card to be %v, but got %v:", Clubs.String(), card.Suit.String())
	}

	// The first card should be of rank Ace
	if deck[0].Rank != Ace {
		card := deck[0]
		test.Errorf("Expected first deck card to be %v, but got %v:", Ace.String(), card.Rank.String())
	}
}

func TestShuffle(test *testing.T) {
	// Create our decks
	deckA := CreateDeck()
	deckB := CreateDeck()

	// Confirm the lengths of both decks match
	if len(deckA) != len(deckB) {
		test.Errorf("Expected same length Deck A (%v) and Deck B (%v) pre-shuffle", len(deckA), len(deckB))
	}

	// Iterate our decks pre-match and confirm they are sequentially identical
	preShuffle := 0
	for idx, cardA := range deckA {
		cardB := deckB[idx]

		if cardA == cardB {
			preShuffle += 1
		}
	}

	// Confirm pre-shuffle match
	if len(deckA) != preShuffle {
		test.Errorf("Expected Deck A length (%v) to equal pre-shuffle match: %v", len(deckA), preShuffle)
	}

	// Confirm the lengths of both decks match
	if len(deckB) != preShuffle {
		test.Errorf("Expected Deck B length (%v) to equal pre-shuffle match: %v", len(deckB), preShuffle)
	}

	// Shuffle both decks so that we can assert unique seed shuffle
	deckA.Shuffle()
	deckB.Shuffle()

	// Confirm the lengths of both decks still match
	if len(deckA) != len(deckB) {
		test.Errorf("Expected same length Deck A (%v) and Deck B (%v) post-shuffle", len(deckA), len(deckB))
	}

	// Iterate our decks and confirm they are sequentially different
	postShuffle := 0
	for idx, cardA := range deckA {
		cardB := deckB[idx]

		// We use match count as there is a chance of occasionally collision
		if cardA == cardB {
			postShuffle += 1
		}
	}

	// Confirm post-shuffle mismatch
	if len(deckA) == postShuffle {
		test.Errorf("Expected Deck A length (%v) not to equal post-shuffle match: %v", len(deckA), postShuffle)
	}

	// Confirm post-shuffle mismatch
	if len(deckB) == postShuffle {
		test.Errorf("Expected Deck B length (%v) not to equal post-shuffle match: %v", len(deckB), postShuffle)
	}
}

func TestDeal(test *testing.T) {
	deck := CreateDeck()

	hand, remainder := deck.Deal(7)

	if len(hand) != 7 {
		test.Errorf("Expected hand to only contain 7 cards, but got: %v", len(hand))
	}

	if len(remainder) != len(deck)-7 {
		test.Errorf("Expected deck remainder to only contain %v cards, but got: %v", len(deck)-7, len(remainder))
	}

	for idx, cardHand := range hand {
		if cardHand != deck[idx] {
			test.Errorf("Expected card in hand (%v) to match card in original deck (%v)", cardHand.Face(), deck[idx].Face())
		}
	}
}

func TestDealHands(test *testing.T) {
	deck := CreateDeck()

	hands, remainder := deck.DealHands(3, 7)

	if len(remainder) != 31 {
		test.Errorf("Expected 31 remaining cards, but got: %v", len(remainder))
	}

	if len(hands) != 3 {
		test.Errorf("Expected 3 hands of cards, but got: %v", len(hands))
	}

	for handIdx, hand := range hands {
		for cardIdx, card := range hand {
			// The deck's relative index is the sum of the current hand index and card index * number of hands
			// e.g. If we have 3 hands total and we are looking at:
			//		- first card of first hand: 0 + (0 * 3) = 0 (the first card of our deck)
			//		- first card of third hand: 2 + (0 * 3) = 2 (the third card of our deck)
			//		- third card of second hand: 1 + (2 * 3) = 7 (the sixth card of our deck)
			relIdx := handIdx + (cardIdx * len(hands))

			if card != deck[relIdx] {
				test.Errorf("Expected Hand #%v Card #%v (%v) to equal Deck Card #%v (%v)", handIdx+1, cardIdx+1, card.Face(), relIdx+1, deck[relIdx].Face())
			}
		}
	}
}
