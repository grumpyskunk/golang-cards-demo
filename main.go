package main

import (
	"cards"
	"fmt"
	"strings"
)

func main() {
	deck := cards.CreateDeck()

	deck.Shuffle()

	hands, deck := deck.DealHands(3, 7)
	players := []string{"Skunk", "Maccs", "Foto"}

	for idx, hand := range hands {
		fmt.Printf("We have %d cards in our hand and %d cards remaining in our deck\n", len(hand), len(deck))

		player := players[idx]
		playerFile := fmt.Sprintf("%s.json", strings.ToLower(player))
		fmt.Printf("Saving %s's hand to disk...\n", player)
		hand.DeckToFile(playerFile, player)

		fmt.Printf("Reading %s's hand from disk...\n", player)
		fromFile := cards.DeckFromFile(playerFile)

		fmt.Printf("--- %s's Hand ---\n", player)
		fromFile.Print()
	}
	fmt.Println(len(deck))
}
